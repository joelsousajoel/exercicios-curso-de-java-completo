/*
* Leia 1 valor inteiro N, que representa o número de casos de teste que vem a seguir. Cada caso de teste consiste
* de 3 valores reais, cada um deles com uma casa decimal. Apresente a média ponderada para cada um destes
* conjuntos de 3 valores, sendo que o primeiro valor tem peso 2, o segundo valor tem peso 3 e o terceiro valor tem
* peso 5.
* */

import java.util.Scanner;
import java.util.Locale;

public class exercicio20 {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);


        int n;
        double v1, v2, v3, mediaPonderada;

        System.out.println("Digite o número de casos: ");
        n = sc.nextInt();

        for (int i = 0; i < n; i++) {

            System.out.println("Digite 3 valores reais");

            v1 = sc.nextDouble();
            v2 = sc.nextDouble();
            v3 = sc.nextDouble();

            mediaPonderada = (v1 * 2 + v2 * 3 + v3 * 5) / 10;

            System.out.printf("%.1f%n", mediaPonderada);

        }

        sc.close();

    }

}
