/*
* Leia 2 valores inteiros (A e B). Após, o programa deve mostrar uma mensagem "Sao Multiplos" ou "Nao sao
* Multiplos", indicando se os valores lidos são múltiplos entre si. Atenção: os números devem poder ser
* digitados emordem crescente ou decrescente.*
*/

import java.util.Scanner;

public class exercicio09 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int a, b;

        System.out.print("Digite o 1º número inteiro: ");
        a = sc.nextInt();
        System.out.print("Digite o 2º número inteiro: ");
        b = sc.nextInt();

        if (a % b == 0 || b % a == 0) {
            System.out.println("São Múltiplos!");
        }
        else {
            System.out.println("Não são Múltiplos!");
        }

        sc.close();
    }
}
