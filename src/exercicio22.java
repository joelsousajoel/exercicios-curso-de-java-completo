/*
* Ler um valor N. Calcular e escrever seu respectivo fatorial. Fatorial de N = N * (N-1) * (N-2) * (N-3) * ... * 1.
* Lembrando que, por definição, fatorial de 0 é 1.
* */

import java.util.Scanner;

public class exercicio22 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n, fatorial = 0;

        System.out.println("Digite um número para calcular seu fatorial: ");
        n = sc.nextInt();

        fatorial = n;

        if (n == 0) {

            System.out.println(1);

        }
        else {

            for (int i = 1; i < n; i++) {
                fatorial = fatorial * i;
            }

            System.out.println(fatorial);

        }

        sc.close();

    }

}
