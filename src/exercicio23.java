/*
* Ler um número inteiro N e calcular todos os seus divisores.
* */

import java.util.Scanner;

public class exercicio23 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n;

        System.out.println("Digite um número inteiro para mostrar seus divisores: ");
        n = sc.nextInt();

        for (int i = 1; i <= n; i++) {

            if ( n%i == 0) {
                System.out.println(i);
            }

        }

        sc.close();

    }

}
