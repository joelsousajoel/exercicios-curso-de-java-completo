/*
* Leia a hora inicial e a hora final de um jogo. A seguir calcule a duração do jogo, sabendo
* que o mesmo pode começar em um dia e terminar em outro, tendo uma duração mínima de 1 hora
* e máxima de 24 horas.
*/

import java.util.Scanner;

public class exercicio10 {

    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);

        int horaInicial, horaFinal, totalHoras;

        System.out.print("Digite a hora inicial do jogo: ");
        horaInicial = sc.nextInt();
        System.out.print("Digite a hora final do jogo: ");
        horaFinal = sc.nextInt();

        if (horaInicial > horaFinal) {
            totalHoras = (horaFinal + 24) - horaInicial;
        }
        else if (horaInicial == horaFinal) {
            totalHoras = 24;
        }
        else {
            totalHoras = horaFinal - horaInicial;
        }

        System.out.printf("O jogo durou %d horas(s)!", totalHoras);

        sc.close();
    }
}
