/*
* Em um país imaginário denominado Lisarb, todos os habitantes ficam felizes em pagar seus impostos,
* pois sabem que nele não existem políticos corruptos e os recursos arrecadados são utilizados em
* benefício da população, sem qualquer desvio. A moeda deste país é o Rombus, cujo símbolo é o R$.
* Leia um valor com duas casas decimais, equivalente ao salário de uma pessoa de Lisarb. Em seguida,
* calcule e mostre o valor que esta pessoa deve pagar de Imposto de Renda, segundo a tabela abaixo.
*
* |=================================|====================|
* |             RENDA               |  IMPOSTO DE RENDA  |
* |=================================|====================|
* |  de R$ 0.00 até R$ 2000.00      |       Isento       |
* |  de R$ 2000.01 até R$ 3000.00   |         8%         |
* |  de R$ 3000.01 até R$ 4500.00   |         18%        |
* |  acima de  R$ 4500.00           |         28%        |
* |=================================|====================|
*/

import java.util.Locale;
import java.util.Scanner;

public class exercicio14 {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        double salario, valorImposto;

        System.out.print("Digite o valor do salário: ");
        salario = sc.nextDouble();

        if (salario < 2000) {
            System.out.println("Isento");
        }
        else if (salario < 3000) {
            valorImposto = 0.08 * (salario - 2000);
            System.out.printf("R$ %.2f", valorImposto);
        }
        else if (salario < 4500) {
            valorImposto =  (0.08 * 1000) + 0.18 * (salario - 3000);
            System.out.printf("R$ %.2f", valorImposto);
        }
        else {
            valorImposto =  (0.08 * 1000) + (0.18 * 1500) + 0.28 * (salario - 4500);
            System.out.printf("R$ %.2f", valorImposto);
        }

        sc.close();
    }

}
