/*
* Fazer um programa que leia o número de um funcionário, seu número de horas trabalhadas,
* o valor que recebe por hora e calcula o salário desse funcionário. A seguir, mostre o
* número e o salário do funcionário, com duas casas decimais.
*/

import java.util.Locale;
import java.util.Scanner;

public class exercicio04 {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        int numeroFuncionario, numeroHoras;
        double valorHora, salario;

        System.out.print("Digite o número do funcionário: ");
        numeroFuncionario = sc.nextInt();
        System.out.print("Digite o número de horas que o funcionário tem: ");
        numeroHoras = sc.nextInt();
        System.out.print("Digite o valor da hora do funcionário: ");
        valorHora = sc.nextDouble();
        salario = numeroHoras * valorHora;
        System.out.printf("Number = %d %nSalary = U$ %.2f%n", numeroFuncionario, salario);

        sc.close();

    }

}
