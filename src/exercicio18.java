/*
* Leia um valor inteiro X (1 <= X <= 1000). Em seguida mostre os ímpares de 1 até X, um valor
* por linha, inclusive o X, se for o caso.
* */

import java.util.Scanner;

public class exercicio18 {

    public static void main(String[] args) {

        Scanner sc =  new Scanner(System.in);

        int x;

        System.out.println("Digite um número inteiro entre 1 e 1000: ");
        x = sc.nextInt();

        while (x < 1 || x > 1000) {
            System.out.println("Número inválido!");
            System.out.println("Digite um número inteiro entre 1 e 1000: ");
            x = sc.nextInt();
        }

        for (int i = 1; i < x + 1; i += 2) {
            System.out.println(i);
        }

        sc.close();

    }

}
