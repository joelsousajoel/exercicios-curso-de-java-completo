/*
 * Faça um programa para ler dois valores inteiros, e depois mostrar na tela a soma desses números com uma
 * mensagem explicativa.
 */

import java.util.Scanner;

public class exercicio01 {

    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);

        int num1, num2, soma;

        System.out.print("Digite o 1º número: ");
        num1 = sc.nextInt();
        System.out.print("Digite o 2º número: ");
        num2 = sc.nextInt();
        soma = num1 + num2;
        System.out.println("Soma = " + soma);

        sc.close();

    }

}
