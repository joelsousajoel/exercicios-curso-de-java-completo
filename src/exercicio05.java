/*
* Fazer um programa para ler o código de uma peça 1, o número de peças 1,
* o valor unitário de cada peça 1, o código de uma peça 2, o número de peças 2
* e o valor unitário de cada peça 2. Calcule e mostre o valor a ser pago.
*/

import java.util.Locale;
import java.util.Scanner;

public class exercicio05 {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        int codigo1, codigo2, num1, num2;
        double valorPeca1, valorPeca2, total;

        System.out.print("Digite o código da peça 1, o nº de peças e o valor de cada peça: ");
        codigo1 = sc.nextInt();
        num1 = sc.nextInt();
        valorPeca1 = sc.nextDouble();

        System.out.print("Digite o código da peça 2, o nº de peças e o valor de cada peça: ");
        codigo2 = sc.nextInt();
        num2 = sc.nextInt();
        valorPeca2 = sc.nextDouble();

        total = num1 * valorPeca1 + num2 * valorPeca2;

        System.out.printf("Valor a pagar: R$ %.2f", total);

        sc.close();


    }
}
