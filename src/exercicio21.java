/*
* Fazer um programa para ler um número N. Depois leia N pares de números e mostre a divisão do primeiro pelo
* segundo. Se o denominador for igual a zero, mostrar a mensagem "divisao impossivel".
* */

import java.util.Scanner;
import java.util.Locale;

public class exercicio21 {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        int rodadas;
        double n1, n2, divisao;

        System.out.println("Digite o número de rodadas: ");
        rodadas = sc.nextInt();

        for (int i = 0; i < rodadas; i++ ) {

            System.out.println("Digite 2 números para fazer a divisão: ");
            n1 = sc.nextDouble();
            n2 = sc.nextDouble();

            if (n2 == 0) {
                System.out.println("Divisão Impossível");
            }
            else {
                divisao = n1 / n2;
                System.out.printf("%.1f%n", divisao);
            }

        }

        sc.close();

    }



}
