/*
* Leia 2 valores com uma casa decimal (x e y), que devem representar as coordenadas
* de um ponto em um plano. A seguir, determine qual o quadrante ao qual pertence o
* ponto, ou se está sobre um dos eixos cartesianos ou na origem (x = y = 0).
* Se o ponto estiver na origem, escreva a mensagem “Origem”.
* Se o ponto estiver sobre um dos eixos escreva “Eixo X” ou “Eixo Y”, conforme for a
* situação.
*/

import java.util.Locale;
import java.util.Scanner;

public class exercicio13 {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        float x, y;
        String quadrante;

        System.out.print("Digite o valor de x e y: ");
        x = sc.nextFloat();
        y = sc.nextFloat();

        if (x > 0 && y > 0) {
            quadrante = "Q1";
        }
        else if (x < 0 && y > 0) {
            quadrante = "Q2";
        }
        else if (x < 0 && y < 0) {
            quadrante = "Q3";
        }
        else if (x > 0 && y < 0) {
            quadrante = "Q4";
        }
        else {
            quadrante = "Origem";
        }

        System.out.println(quadrante);

        sc.close();






    }

}
