/*
* Escreva um programa que repita a leitura de uma senha até que ela seja válida. Para cada leitura de senha
* incorreta informada, escrever a mensagem "Senha Invalida". Quando a senha for informada corretamente deve ser
* impressa a mensagem "Acesso Permitido" e o algoritmo encerrado. Considere que a senha correta é o valor 2002.
* */

import java.util.Scanner;

public class exercicio15 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int senha = 2002, senhaDigitada;

        System.out.print("Digite uma senha numérica: ");
        senhaDigitada = sc.nextInt();

        while (senhaDigitada != senha) {
            System.out.println("Senha Inválida!");
            System.out.println("Digite uma senha numérica: ");
            senhaDigitada = sc.nextInt();
        }

        System.out.println("Acesso Permitido!");

        sc.close();


    }

}
